transmission_repo:
  pkgrepo.managed:
    - name: ppa:transmissionbt/ppa

transmission_packages:
  pkg.installed:
    - pkgs:
      - transmission-cli
      - transmission-common
      - transmission-daemon

transmission_service:
  service.running:
    - name: transmission-daemon
    - enable: True
    - require:
      - pkg: transmission_packages

transmission_service_dead:
  service.dead:
    - name: transmission-daemon
    - require:
      - pkg: transmission_packages

transmission_settings:
  file.managed:
    - name: /var/lib/transmission-daemon/info/settings.json
    - source: salt://transmission/settings.json
    - user: root
    - group: root
    - template: jinja
    - mode: 777
    - require:
      - pkg: transmission_packages
      - service: transmission_service_dead
    - watch_in:
      - service: transmission_service

transmission_downloads:
  file.directory:
    - name: {{ salt['pillar.get']('transmission:download', '/var/lib/transmission-daemon/downloads') }}
    - user: debian-transmission

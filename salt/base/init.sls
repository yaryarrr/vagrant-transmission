vim_package:
  pkg.installed:
    - name: vim

# iptables_service:
#   service.dead:
#     - name: iptables

# ufw_service:
#   service.dead:
#     - name: ufw

locate_package:
  pkg.installed:
    - name: mlocate

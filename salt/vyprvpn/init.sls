openvpn_package:
  pkg.installed:
    - name: openvpn

vyprvpn_cert_download:
  cmd.run:
    - name: "sudo wget -O /etc/openvpn/ca.vyprvpn.com.crt https://www.goldenfrog.com/downloads/ca.vyprvpn.com.crt"
    - unless: "cat /etc/openvpn/ca.vyprvpn.com.crt"
  file.managed:
    - name: /etc/openvpn/ca.vyprvpn.com.crt
    - mode: 600
    - require:
      - cmd: vyprvpn_cert_download

vyprvpn_configuration:
  file.managed:
    - name: /etc/openvpn/vyprvpn.conf
    - source: salt://vyprvpn/vyprvpn.conf
    - user: root
    - group: root
    - template: jinja
    - mode: 600
    - require:
      - pkg: openvpn_package
      - file: vyprvpn_userpass
    - watch_in:
      - service: openvpn_service

openvpn_service:
  service.running:
    - name: openvpn
    - enable: True
    - require:
      - pkg: openvpn_package

vyprvpn_userpass:
  file.managed:
    - name: /etc/openvpn/login.txt
    - source: salt://vyprvpn/login.txt
    - user: root
    - group: root
    - template: jinja
    - mode: 600
    - require:
      - pkg: openvpn_package

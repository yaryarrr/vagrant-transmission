transmission:
  user: transmission
  pass: password
  whitelist: "false"
  download: /var/lib/transmission-daemon/downloads
  auth: "false"

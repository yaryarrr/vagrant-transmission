# RPM Maker
This VM brings up a CentOS 6.5 instance with [fpm](https://github.com/jordansissel/fpm) pre-installed. This VM can then be used to packages.

## Getting Started
Run the following commands:

```shell
$> git clone
...
$> cd rpmmaker
$> vagrant up
...
$> vagrant ssh
```

## Dependencies
[Vagrant](http://www.vagrantup.com) >=1.2.7

[VirtualBox](https://www.virtualbox.org) >=4.2.16
